import { LitElement, html, css } from 'lit-element';

class Movimientos  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
        idusuario:{type:Number},
        resultados:{type:Object},
        campos:{type:Array},
        filas:{type:Array},
        id:{type:Number},
        descipcion:{type:String},
        monto:{type:Number},
        status:{type:String},
        tipo_movimiento: {type:String},
        fecha: {type:Number}
    };
  }

  constructor() {
    super();
    this.idusuario = 1;
    this.campos = [];
    this.filas = [];
    this.resultados={
      results : [
      {
      "_id": {
      "timestamp": 1619190985,
      "date": "2021-04-23T15:16:25.000+00:00"
      },
      "idMovimiento": "3",
      "idUsuario": "reddalex",
      "fechaMovimiento": "22-04-2021",
      "horaMovimiento": "09:00:00",
      "descripcion": "Compara Oxxo",
      "tipoMovimineto": "Cargo",
      "status": "A",
      "monto": 500
      },
      {
      "_id": {
      "timestamp": 1619191205,
      "date": "2021-04-23T15:20:05.000+00:00"
      },
      "idMovimiento": "4",
      "idUsuario": "reddalex",
      "fechaMovimiento": "22-04-2021",
      "horaMovimiento": "09:55:00",
      "descripcion": "Compara 7elven",
      "tipoMovimineto": "Cargo",
      "status": "A",
      "monto": 200.1
      }
      ]};
      this.montarResultados();
  }

  render() {
    return html`
      <link rel="stylesheet"  href="./src/css/movimientos.css" />
      <table class="TablaMovimientos">
      <thead>
          <tr>
              <th># Movimiento</th><th>Fecha Movimiento</th><th>Hora Movimiento</th><th>Descripción</th><th>Tipo Movimiento</th><th>Estatus</th><th>Monto</th><th>Historico</th>
          </tr>
      </thead>
          ${this.filas.map(i => html`<tr>${this.resultados.results[i].valores.map( v => html`<td>${v}</td>`)}
          <td><button @click="${this.archHist}" value=${this.resultados.results[i].valores[0]}> Archivar </button></td>
          </tr>`)}
      </table>
    `;
  }
  archHist(e){
    alert("Click en archivo de movimiento: "+e.tarjet);
    this.montarResultados();
  }
  getUsrMov(){
    fetch("http://localhost:8080/grupo2back/movimientos/"+this.idusuario)
    .then( response => {
        console.log(response); 
        if(!response.ok){ throw response;}
        else{return response.json();}
    })
    .then(data => {
      this.resultados = data;
      this.montarResultados();
    })
    .catch(error => {
        alert("Problemas con el fetch Movimientos: "+error);
        location.reload();
    })
  }
  montarResultados(){
    this.campos = this.getObjProps(this.resultados.results[0]);
    this.filas = [];
    for (var i=0; i < this.resultados.results.length; i++)
    {
        this.resultados.results[i].valores = this.getValores(this.resultados.results[i]);
        this.filas.push(i);
    }
  }
  getValores(fila){
    var valores = [];
    for(var i=0;i< this.campos.length;i++){
        valores[i]= fila[this.campos[i]];
    }
    return valores;
  }
  getObjProps(obj){
    var props = [];
    for (var prop in obj){
        if(!this.isArray(this.resultados.results[0][prop])){
            if((prop !== "_id") && (prop !== "idUsuario")){
                props.push(prop);
            }
        }
    }
    return props;
  }
  isArray(prop){
    return Object.prototype.toString.call(prop) === "[object Array]"
  }   
}

customElements.define('movimientos-page', Movimientos);