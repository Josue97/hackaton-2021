import { LitElement, html, css } from 'lit-element';
import './movimientos.js';
import './editar-usuario.js';

class Landing  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
        navpadding:{ attribute: true},
        logowidth:{ attribute: true},
        topdiv:{attribute: true},
        nombreusuario:{type:String},
        psusuario:{type:String},
        usuario:{type:Object}
    };
  }

  constructor() {
    super();
    this.navpadding = "25px 10px";
    this.logowidth = "150px";
    this.topdiv = "105px";
    this.nombreusuario = "";
    this.aliasusuario = "";
    this.psusuario = "";
  }

  render() {
    return html`
    <link rel="stylesheet"  href="./src/css/landing.css" />
    <div class="container"> 
        <div class="content">
          <div class="bgimage">
        </div> 
          <div id="navbar" class="navbar">
              <img id="logo" class="logoNav" src="./src/img/logo_bbva_blanco.svg" alt="Logo BBVA" />
              <div class="navbar-right">
                <a id="accion1" class="active" @click="${this.valida_a}">Home</a>
                <a id="accion2" @click="${this.valida_b}">Movimientos</a>
                <a id="accion3" @click="${this.valida_c}">Usuarios</a>
                <a id="accion4" @click="${this.valida_d}">
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
                    <path fill="none" d="M0 0h24v24H0z"/>
                    <path class="logo-icon" d="M12 22C6.477 22 2 17.523 2 12S6.477 2 12 2a9.985 9.985 0 0 1 8 4h-2.71a8 8 0 1 0 .001 12h2.71A9.985 9.985 0 0 1 12 22zm7-6v-3h-8v-2h8V8l5 4-5 4z" />
                  </svg>
              </a>
              </div>
          </div>
          <div class="showTime" id="showTime">
            <div class="saludo-centrado" id="bienvenido">
              <img class="logo-persona" src="./src/img/logo_home.svg" alt="Logo BBVA" />
              <h1 class="saludo">¡Bienvenido!</h1>
              <h2 class="nombre">${this.nombreusuario}</h2>
            </div>
          </div>
          <div class="footer">
            <Strong>BBVA Mexico.</Strong> Página de Prueba <strong><i>"Landing Page"</i></strong>. Elaborado para curso "Practitioner Front-End" y "Practitioner Back-End"
          </div>
        </div>
    </div>
    `;
  }
  valida_a(){
    this.shadowRoot.querySelector("#accion1").classList = "active";
    this.shadowRoot.querySelector("#accion2").classList = "";
    this.shadowRoot.querySelector("#accion3").classList = "";
    this.shadowRoot.querySelector("#accion4").classList = "";
    this.setWCHome();
  }
  valida_b(){
    this.shadowRoot.querySelector("#accion1").classList = "";
    this.shadowRoot.querySelector("#accion2").classList = "active";
    this.shadowRoot.querySelector("#accion3").classList = "";
    this.shadowRoot.querySelector("#accion4").classList = "";
    this.setWCMovimientos();
  }
  valida_c(){
    this.shadowRoot.querySelector("#accion1").classList = "";
    this.shadowRoot.querySelector("#accion2").classList = "";
    this.shadowRoot.querySelector("#accion3").classList = "active";
    this.shadowRoot.querySelector("#accion4").classList = "";
    this.setWCUsuarios();
  }
  valida_d(){
    this.shadowRoot.querySelector("#accion1").classList = "";
    this.shadowRoot.querySelector("#accion2").classList = "";
    this.shadowRoot.querySelector("#accion3").classList = "";
    this.shadowRoot.querySelector("#accion4").classList = "active";
    this.logout();
  }
  attributeChangedCallback(name,oldval,newval){
    var navBar = this.shadowRoot.querySelector("#navbar");
    var logo = this.shadowRoot.querySelector("#logo");
    var divS = this.shadowRoot.querySelector("#showTime");
    if(name === "navpadding"){
      navBar.style.padding = newval;
    }
    if(name === "logowidth"){
      logo.style.width = newval;
    }
    if(name === "topdiv"){
      divS.style.marginTop = newval;
    }
    super.attributeChangedCallback(name,oldval,newval);
  }
  setWCHome(){
    //this.getWCInfo();
    var divShow = this.shadowRoot.querySelector("#showTime");
      divShow.innerHTML = `<div class="saludo-centrado" id="bienvenido">
      <img class="logo-persona" src="./src/img/logo_home.svg" alt="Logo BBVA" />
      <h1 class="saludo">¡Bienvenido!</h1>
      <h2 class="nombre">${this.nombreusuario}</h2>
    </div>`;
  }
  setWCMovimientos(){
    var divShow = this.shadowRoot.querySelector("#showTime");
    divShow.innerHTML = `<movimientos-page idusuario=1></movimientos-page>`; 
  }
  setWCUsuarios(){
    var divShow = this.shadowRoot.querySelector("#showTime");
    divShow.innerHTML = `<editar-usuario></editar-usuario>`; 
  }
  getWCInfo(){
    this.usuario = JSON.parse(window.sessionStorage.getItem("dUsr"))[0];
    this.nombreusuario = this.usuario.nombre +" "+ this.usuario.aPaterno +" "+this.usuario.aMaterno;
  }
  logout(){
    window.sessionStorage.setItem("dUsr","");
    location.href = "./"
  }
}

customElements.define('landing-page', Landing);