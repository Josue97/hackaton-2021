import { LitElement, html, css } from 'lit-element';
import { openWcLogo } from './open-wc-logo.js';

export class Hackaton2021 extends LitElement {
  static get properties() {
    return {
      title: { type: String },
      navStylePadding: {type:String},
      logoStyleFontS: {type:String}
    };
  }

  static get styles() {
    return css`
      .bgimage {
        background-image: url("./src/img/bg-bbva.png");
        background-repeat: no-repeat;
        background-attachment: fixed;
        min-height: 920px;
        height:100%;
        background-position: center;
        background-size: cover;
        filter:opacity(50%);
      }
      #navbar {
        overflow: hidden;
        background-color: #f1f1f1;
        padding: 90px 10px;
        transition: 0.4s;
        position: fixed;
        width: 100%;
        top: 0;
        z-index: 99;
      }

      #navbar a {
        float: left;
        color: black;
        text-align: center;
        padding: 12px;
        text-decoration: none;
        font-size: 18px; 
        line-height: 25px;
        border-radius: 4px;
      }

      #navbar #logo {
        font-size: 35px;
        font-weight: bold;
        transition: 0.4s;
      }

      #navbar a:hover {
        background-color: #ddd;
        color: black;
      }

      #navbar a.active {
        background-color: dodgerblue;
        color: white;
      }

      #navbar-right {
        float: right;
      }

      @media screen and (max-width: 580px) {
        #navbar {
          padding: 20px 10px !important;
        }
        #navbar a {
          float: none;
          display: block;
          text-align: left;
        }
        #navbar-right {
          float: none;
        }
      }
    `;
  }

  constructor() {
    super();
    this.title = 'My app';
    this.navStylePadding = "80px 10px";
    this.logoStyleFontS = "35px";
  }

  render() {
    return html`
      <main>
        <div class="bgimage">
        </div>
        <div class="container">
          <div id="navbar" style="${this.navStylePadding}">
            <a href="#default" id="logo" style="${this.logoStyleFontS}">CompanyLogo</a>
            <div id="navbar-right">
              <a class="active" href="#home">Home</a>
              <a href="#contact">Contact</a>
              <a href="#about">About</a>
            </div>
          </div>
        </div>
      </main>

      <p class="app-footer">
        🚽 Made with love by
        <a
          target="_blank"
          rel="noopener noreferrer"
          href="https://github.com/open-wc"
          >open-wc</a
        >.
      </p>
    `;
  }
}
