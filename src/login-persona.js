import { LitElement, html, css } from 'lit-element';


class LoginPersona  extends LitElement {

  static get styles() {
    return css`
    
    `;
  }

  static get properties() {
    return {
      userName:{type:String},
        password:{type:String},
        usuario:{type:String}
    };
  }

  constructor() {
    super();
    this.userName = "";
    this.password= "";
    this.usuario = "";
}


  render() {
    return html`
    <link rel="stylesheet"  href="./src/css/login.css" />
    <div class="container">
        <div class="bgimage">
        </div>  
        <div class="login-div">
          <br/>
          <img class="logo" src="./src/img/logo_bbva_azul.svg" alt="Logo BBVA" />
          <h2 class="h2-center">Bienvenido a BBVA</h2>
          <div class="inline">
            <div class="usr">
            <img class="logo-icon" src="./src/img/logo_usuario.svg" alt="Logo Usuario" />
            <input class="input-form" type="text" id="iuser" name="iuser" value="${this.userName}" @input="${this.guardarUser}" />
            </div>
            <br/>
            <img class="logo-icon" src="./src/img/logo_password.svg" alt="Logo Password" />
            <input class="input-form" type="password" id="ipassword" name="ipassword" value="${this.password}" @input="${this.guardarPassword}" />
            <br/>
            <br/>
            <button class="button-login" @click="${this.login}">Ingresar</button>
          </div>
        </div>  
      </div> 
    `;
  } 
  guardarUser(e){
    this.userName=e.target.value;
    this.usuario[0] = this.userName;
  } 
  guardarPassword(e){
    this.contraseña=e.target.value;
    this.usuario[1]=this.contraseña;
  }
  getCurrentUser(){
    var usuario = {};
    usuario.userName = this.userName;
    usuario.password = this.contraseña;
    return usuario;
  }
  login(){
      var usuario = this.getCurrentUser();
      const options = {
          method: 'POST',
          body:JSON.stringify(usuario),
          headers:{'Content-Type':"application/json"}
      }
    fetch("http://localhost:8081/api/users/user/login/",options)
    .then( response => {
        console.log(response); 
        if(!response.ok){ throw response;}
        else{return response.json();}
    })
    .then(data => {
      this.usuario=JSON.stringify(data);
      window.sessionStorage.setItem("dUsr",this.usuario);
      location.href = "./landing.html";
    })
    .catch(error => {
        alert("Problemas con el login: "+error);
        window.sessionStorage.setItem("dUsr","");
        location.reload();
    })
  } 
}

customElements.define('login-persona', LoginPersona);