import { LitElement, html, css } from 'lit-element';

export class EditarUsuario  extends LitElement {

  static get styles() {
    return css`
      div {
        border-radius: 10px;
        padding: 30px;
        margin: 30px;;
        background-color: white;
        top: 50%;
        left: 50%;
        width: 350px;
        -ms-transform: translate(-60%, -60%);
        transform: translate(-60%, -60%);
        border-radius: 10px;
        padding: 30px;
        position: absolute;
        transition: box-shadow 0.6s ease-out 100ms
      }
      div:hover{
          border: 1px solid;
          border-color: #49a5e6;
          box-shadow: 0px 0px 15px 5px #49a5e6;
      }
      input{
        width:60%;
        color:#49a5e6;
        margin-left:25px;
        border-width: 0 0 1px;
        border-color: #49a5e6;
        outline-width: 0;
        transition: all 0.2s ease-out 100ms
      }
     input:hover{
        border-width: 0 0 2px;
      }
      input:focus{
        border-width: 0 0 2px;
      }
    `;
  }

  static get properties() {
    return {
        ID:{type:Number},
        password:{type:String},
        fecha:{type:Date},
        status:{type:String},
        username:{type:String},
        nombre:{type:String},
        apaterno:{type:String},
        amaterno:{type:String},
        fnacimiento:{type:Date}
    };
  }

  constructor() {
    // this.ID= 0;
    //fecha= new Date();
    super();
  }

  render() {
    return html`
    <div>
    <br/>
    <h3>CRUD usuario</h3>
    <br/>
    <label>Id</label>
    <input type="text" value="123456"/>
    <br/>
    <label>password</label>
    <input type= "password" @input="${this.checarPassword}"/>
    <br/>
    <label>fechaAct</label>
    <input type="date" value="${new Date().toLocaleString("es-MX","aa-mm-yyyy")}"/>
    <br>
    <label>status</label>
    <input type= "text">
    <br/>
    <label>Username</label>
    <input type= "text" @input="${this.guardarUsername}"/>
    <br>
    <label>Nombre</label>
    <input type="text" @input="${this.guardarNombre}"/>
    <br/>
    <label>Apellido Paterno</label>
    <input type= "text" @input="${this.guardarApaterno}"/>
    <br/>
    <label>Apellido Materno</label>
    <input type= "text" @input="${this.guardarAmaterno}"/>
    <br/>
    <label>fecha de nacimiento</label>
    <input type= "date" @input="${this.guardarFnacimiento}"/>
    <br/>   
    <button>Dar de alta usuario</button>

    </div>
    
      
    `;
  }
  checarPassword(e){
    this.password=e.target.value;
  }
  guardarUsername(e){
    this.username=e.target.value;
  } 
  guardarNombre(e){
    this.nombre=e.target.value;
  } 
  guardarApaterno(e){
    this.apaterno=e.target.value;
  } 
  guardarAmaterno(e){
    this.amaterno=e.target.value;
  } 
  guardarFnacimiento(e){
    this.fnacimiento=e.target.value;
  }  
}

customElements.define('editar-usuario', EditarUsuario);